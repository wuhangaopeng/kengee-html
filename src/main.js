// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import Main from '@/components/main/main'
import Error from '@/components/error/error'
import WechatError from '@/components/error/notwechat'
import Exist from '@/components/exist/exist'
import Success from '@/components/success/success'
import utils from './utils/utils.js'

// require('./assets/vconsole.js')
Vue.prototype.$utils=utils

const domain = "http://demo.whgaopeng.com/"
// const domain = "http://localhost:8080/"
Vue.prototype.$domain = domain
Vue.use(VueRouter)
//创建routes
const routes=[
  {path:"/",redirect: "/main"},
  {path:"/main",component:Main},
  {path:"/error",component:Error},
  {path:"/exist",component:Exist},
  {path:"/notwechat",component:WechatError},
  {path:"/success",component:Success}
]
const router = new VueRouter({
  mode:'history',
  routes,
  linkActiveClass:"active"
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
